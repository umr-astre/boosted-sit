# Boosted Sterile Insect Technique

J Bouyer, R Lancelot, T Baldet, F Muñoz \
February 10, 2022

Effect of boosted SIT on the apparent density of Ae. aegypti in La Reunion
Island: a field experiment.

Boosted SIT: Release sterile male mosquitoes coated with piriproxyfen

Apparent density: Size of the __captured__ population. Specifically, log-number
of captured individuals per trap per day.

The objective is to assess whether "boosting" (i.e. using the coating) has some
noticeable impact on the decline of the population after a SIT intervention.

Since we cannot quantify the entire natural population directly, we consider
the _apparent density_ as a proxy.

- [report](https://umr-astre.pages.mia.inra.fr/boosted-sit/Release_BGT.html)

## How to use

- Open the RStudio project (or a R session with the working directory set at the
project's root).

- Run `targets::tar_make()` to build/update all the _targets_ and cache them.

- PDF reports are compiled into `reports/`, HTML versions of reports and slides
are in `public/`.

- Use `targets::tar_load(everything())` to load targets into memory for interactive
examination.
