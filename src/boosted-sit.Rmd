---
title: "boosted-sit"
author: "Facundo Muñoz"
date: "`r format(Sys.Date(), '%e %B, %Y')`"
output:
  pdf_document:
    template: null
documentclass: cirad
# citation_package:
#   natbib:
#     natbiboptions: "numbers, sort&compress"
# csl: plos-one.csl	
# bibliography: boosted-sit.bib
---


```{r eval = interactive(), cache = FALSE, include = FALSE}
source("src/packages.R")
source("src/functions.R")
message("Assuming interactive session")
```

```{r setup, include=FALSE, cache = FALSE}
knitr::opts_chunk$set(
  echo = FALSE,
  cache = FALSE,
  dev = ifelse(is_latex_output(), "cairo_pdf", "png"),
  dpi = 300
)

theme_set(theme_ipsum())
```


# Introduction


```{r load, include = FALSE}
tar_load(
  c(
    clean_data
  )
)
```



\clearpage

# Data description


\clearpage

# TODO

- ...



# Conclusions

- ...
