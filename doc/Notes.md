---
title: "Boosted SIT - La Réunion"
author: Facundo Muñoz, Renaud Lancelot
date: "`r format(Sys.Date(), '%b, %Y')`"
output:
  pdf_document: default
  html_document: default
documentclass: cirad
---



## Context

E-mail from R Lancelot 2022-02-01

Voici ce que je suis en train de faire sur les données  boosted SIT "moustiques adultes" de la Réunion. 

Il y a 2 autres jeux de données:

1) données ovitrap : nbre d'oeufs collectés et probabilité de survie des stades immatures jusqu'à émergence des adultes néonates

2) dosage du pyriproxyfen (PP: la composant "boosted" de boosted SIT) dans l'eau des ovitraps

Le PP est un inhibiteur de croissance larvaire utilisé sous forme de poudre appliquée sur les mâles stériles juste avant de les lâcher. Quand ils s'accouplent, ils en transfèrent une partie sur les femelles. Celles-ci contaminent à leur tour les sites de ponte: si la concentration du PP est suffisante, elle inhibe la coissance larvaire.

Jérémy a placé des ovitrap (oviposition traps) près de ces sites. Il a collecté le nb d'oeufs pondus, leur survie, ainsi que la concentration en PP dans l'eau des ovitraps.

Je commence l'analyse des données ovitrap et PP.

Pourrais-tu s'il tr plaît mettre en place un projet "Boosted SIT" sur Github, de manière à sécuriser les données et les rendre accessibles aux personnes concernées?

## Data



## Proposed Analyses



## Remarks

